﻿class Ball extends CanvasObject {
    constructor(src, x, y, width, height, radians) {
        super(src, x, y, width, height);
        this.radians = radians;
        this.setMinis(width, height);
    }

    changeImage(src, width, height) {
        super.changeImage(src, width, height);
        this.setMinis(width, height);
    }

    changeSize(width, height) {
        this.height = height;
        this.width = width;
    }

    setMinis(w, h) {
        this.miniWidth = w - 2;
        this.miniHeight = h - 2;
    }

    changePosition(x, y, radians) {
        super.changePosition(x, y);
        this.radians = radians;
    }

    move(x, y) {
        var oldX = this.x;
        var oldY = this.y;

        this.x += -x * 5.0;
        this.y += y * 5.0;

        if (this.x > playground.width - this.width) {
            this.x = playground.width - this.miniWidth;
        } else if (this.x < this.width) {
            this.x = this.miniWidth;
        }

        if (this.y > playground.height - this.height) {
            this.y = playground.height - this.miniHeight;
        } else if (this.y < this.height) {
            this.y = this.miniHeight;
        }

        this.radians = Math.atan2(this.x - oldX, oldY - this.y).toFixed(1);
    }

    draw() {
        super.draw(this.radians);
    }
}