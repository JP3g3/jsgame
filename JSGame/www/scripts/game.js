﻿
const playground = document.getElementById("playground");
const playgroundContext = playground.getContext("2d");
const winBox = document.getElementById("winBox");
const loseBox = document.getElementById("loseBox");
const endBox = document.getElementById("endBox");
const timer = document.getElementById("timer");
const level = document.getElementById("level");

var knifes = [];
var times = [];
var speed = 3;
var lvl = 0;
var k = 0;
var endAnimationIndex = 0;
var lostAnimationIndex = 0;

var accelerometerInterval;
var timeInterval;
var start;
var home;
var ball;

function onStart() {
    knifes = [];
    times = [];
    playground.width = window.innerWidth;
    playground.height = window.innerHeight;

    home = new CanvasObject("images/stew.png", playground.width / 2, 120, 64, 64);
    ball = new Ball("images/vegetables/0.png", playground.width / 2, playground.height - 100, 32, 32, 0.0);
    knifes.push(new Knife(34, 250, 0.0, true));
    setIntervals(true);
    animate();
}

function reset() {
    setIntervals(false);
    endAnimationIndex = 0;
    lostAnimationIndex = 0;
    knifes[0].resetData(34, 250, 0.0, true);
    ball.changePosition(playground.width / 2 - 16, playground.height - 100, 0.0);

    if (lvl > 3) {
        ball.changeImage("images/vegetables/4.png", 16, 32);

    } else {
        ball.changeImage("images/vegetables/" + lvl + ".png", 32, 32);
    }

    switch (lvl) {
        case 1:
            if (lvl === knifes.length - 1) {
                knifes[1].resetData(playground.width - 34, 380, 3.1, false);

            } else {
                knifes.push(new Knife(playground.width - 34, 380, 3.1, false));
            }
            break;

        case 2:
            speed = 4;
            knifes[1].resetData(playground.width - 34, 380, 3.1, false);
            break;

        case 3:
            speed = 3;
            knifes[1].resetData(playground.width - 34, 380, 3.1, false);
            if (lvl === knifes.length - 1) {
                knifes[2].resetData(playground.width - 34, 250, 3.1, false);
                knifes[3].resetData(34, 380, 0.0, true);

            } else {
                knifes.push(new Knife(playground.width - 34, 250, 3.1, false));
                knifes.push(new Knife(34, 380, 0.0, true));
            }
            break;

        case 4:
            speed = 5;
            knifes[1].resetData(playground.width - 34, 380, 3.1, false);
            knifes[2].resetData(playground.width - 34, 250, 3.1, false);
            knifes[3].resetData(34, 380, 0.0, true);
            break;
    }
    loseBox.style.display = "none";
    winBox.style.display = "none";
    setIntervals(true);
}

function increaseLevel() {
    lvl += 1;
    reset();
}

function moveBall(acceleration) {
    ball.move(acceleration.x, acceleration.y);
    animate();
}

function animate() {
    playgroundContext.clearRect(0, 0, playground.width, playground.height);
    home.draw();
    ball.draw();

    for (k in knifes) {
        knifes[k].moveKnife();
        knifes[k].draw();
    }

    if (collisionBetweenCircle(home, ball)) {
         setIntervals(false);
         saveTime();
         endAnimation();
    }

    for (k in knifes) {
        if (collides(knifes[k], ball)) {
            setIntervals(false);
            navigator.vibrate(1000);
            lostAnimation();
        }
    }
}

function endAnimation() {
    if (endAnimationIndex === 0) {
        endAnimationIndex++;

        var w = ball.width;
        var h = ball.height;
        var halfWidth = playground.width / 2;

        var interval = setInterval(function () {
            if (ball.width <= 2) {
                ball.width = w;
                ball.height = h;
                clearInterval(interval);

                playgroundContext.clearRect(0, 0, playground.width, playground.height);
                home.draw();

                var time = 0;

                if (lvl === 4) {
                    time = countTimes().toFixed(1);
                    endBox.style.display = "block";
                    document.getElementById("gameTime").innerText = "You have finished in " + time + " seconds";
                    saveTimeInDBOnlyOnce();

                    setTimeout(function () {
                        window.history.back();
                    }, 3000);

                } else {
                    time = times[lvl].toFixed(1);
                    winBox.style.display = "block";
                    document.getElementById("levelTime").innerText = "Time " + time + " seconds";
                }
                return;
            }

            var x = ball.x;
            var y = ball.y;

            ball.x += x > halfWidth ? -2 : 2;
            ball.y += y > 120 ? -2 : 2;

            playgroundContext.clearRect(0, 0, playground.width, playground.height);
            home.draw();
            ball.draw();
            for (k in knifes) {
                knifes[k].draw();
            }
            ball.width -= 1;
            ball.height -= 1;
        }, 50);
    }
}

function lostAnimation() {
    if (endAnimationIndex === 0) {
        endAnimationIndex++;

        var splash = new CanvasObject("images/splash/" + lvl + ".png", ball.x, ball.y, 16, 16);

        var interval = setInterval(function () {
            if (splash.width > 60) {
                clearInterval(interval);
                loseBox.style.display = "block";
                return;
            }

            playgroundContext.clearRect(0, 0, playground.width, playground.height);
            home.draw();
            for (k in knifes) {
                knifes[k].draw();
            }
            splash.draw();
            splash.width += 2;
            splash.height += 2;
        }, 50);
    }
}

var saveTimeInDBOnlyOnce = (function () {
    var executed = false;
    return function () {
        if (!executed) {
            executed = true;
            var time = countTimes().toFixed(1);
            saveTimeInDB(time);
        }
    };
})();

function saveTime() {
    var now = new Date().getTime();
    var time = -(start - 60000 - now) / 1000;
    times[lvl] = time;
}

function countTimes() {
    var counter = 0;
    for (var t in times) {
        counter += times[t];
    }
    return counter;
}

function collides(a, b) {
    return a.x < b.x + b.width && a.x + a.width > b.x && a.y < b.y + b.height && a.y + a.height > b.y;
}

function collisionBetweenCircle(a, b) {
    var xx = b.x - a.x;
    var yy = b.y - a.y;
    var distance = Math.sqrt((xx * xx) + (yy * yy));

    if (distance < ((a.width / 2) + (b.width / 2) - 6)) {
        return true;
    } return false;
}

function setIntervals(flag) {
    if (flag) {
        var l = lvl + 1;
        level.innerText = "lvl: " + l;
        start = new Date().getTime() + 60000;
        timeInterval = setInterval(timeIntervalFunc, 1000);
        accelerometerInterval = setInterval(accelerometerIntervalFunc, 40);

    } else {
        clearInterval(timeInterval);
        clearInterval(accelerometerInterval);
    }
}

function accelerometerIntervalFunc() {
    if (navigator.accelerometer) {
        navigator.accelerometer.getCurrentAcceleration(moveBall, () => { console.log("error"); });
    }
}

function timeIntervalFunc() {
    var now = new Date().getTime();
    var distance = start - now;
    var seconds = Math.floor(distance % (1000 * 60) / 1000);

    timer.style.color = seconds < 10 ? "red" : "white";
    timer.innerHTML = "Time: " + seconds;
    if (seconds < 1) {
        setIntervals(false);
        loseBox.style.display = "block";
    }
}