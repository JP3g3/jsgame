﻿
var config = {
    apiKey: "AIzaSyDgQCQCO9sHnrNZvP2J_Q9cnWHK5xurU58",
    authDomain: "jsgame-12.firebaseapp.com",
    databaseURL: "https://jsgame-12.firebaseio.com",
    projectId: "jsgame-12",
    storageBucket: "",
    messagingSenderId: "1013684475965"
};

firebase.initializeApp(config);

var ref = firebase.database().ref('times');
var name = localStorage.getItem('NAME');
const LIMIT = 14;

function getData() {
    let scores = document.getElementById("scores");
    var lastTime = 0;
    var counter = 0;
    var place = 0;

    ref.orderByChild("time").once('value', function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var data = childSnapshot.val();
            var time = data.time;

            if (counter > LIMIT) return true;
            if (lastTime !== time) place++;
            scores.innerHTML += "<tr><td>" + place + "</td><td>" + time + "s</td><td>" + data.user + "</td></tr>";
            lastTime = time;
            counter++;
        });
    });
}

function saveTimeInDB(time) {
    name = name !== "null" ? name : "anonymous";
    time = parseFloat(time);

    var newRef = ref.push();
    newRef.set({ "time": time, "user": name });
}