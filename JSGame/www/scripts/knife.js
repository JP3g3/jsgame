﻿class Knife extends CanvasObject {
    constructor(x, y, radians, rightDirect) {
        super("images/knife.png", x, y, 8, 64);

        this.radians = radians;
        this.rightDirect = rightDirect;
    }

    resetData(x, y, radians, rightDirect) {
        this.x = x;
        this.y = y;
        this.radians = radians;
        this.rightDirect = rightDirect;
    }
  
    moveKnife() {
        this.x += this.rightDirect ? speed : -speed;
        this.radians += 0.0174;

        if (this.x >= playground.width - 40) {
            this.rightDirect = false;

        } else if (this.x <= 40) {
            this.rightDirect = true;
        }
    }

    draw() {
        super.draw(this.radians);
    }

    collision(circle) {
        var rectCenterX = this.x + this.width / 2;
        var rectCenterY = this.y + this.height / 2;

        var rotateCircleX = Math.cos(-this.radians) * (circle.x - rectCenterX) - Math.sin(-this.radians) * (circle.y - rectCenterY) + rectCenterX;
        var rotateCircleY = Math.sin(-this.radians) * (circle.x - rectCenterX) + Math.cos(-this.radians) * (circle.y - rectCenterY) + rectCenterY;

        var cx = rotateCircleX;
        var cy = rotateCircleY;

        if (rotateCircleX < this.x) {
            cx = this.x;
        } else if (rotateCircleX > this.x + this.width) {
            cx = this.x + this.width;
        }

        if (rotateCircleY < this.y) {
            cy = this.y;
        } else if (rotateCircleY > this.y + this.height) {
            cy = this.y + this.height;
        }

        if (this.distance(rotateCircleX, rotateCircleY, cx, cy) < (circle.height / 2)) {
            return true;
        }
        return false;
    }

    distance(fromX, fromY, toX, toY) {
        var x = fromX - toX;
        var y = fromY - toY;

        return Math.sqrt((x * x) + (y * y));
    }
}