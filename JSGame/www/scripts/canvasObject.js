﻿class CanvasObject {
    constructor(src, x, y, width, height) {
        this.image = new Image();
        this.image.src = src;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    changePosition(x, y) {
        this.x = x;
        this.y = y;
    }

    changeImage(src, width, height) {
        this.image.src = src;
        this.width = width;
        this.height = height;
    }

    draw() {
        playgroundContext.drawImage(this.image, this.x, this.y, this.width, this.height);
    }

    draw(radians) {
        playgroundContext.save();
        playgroundContext.translate(this.x, this.y);
        playgroundContext.rotate(radians);
        playgroundContext.translate(-this.x, -this.y);

        playgroundContext.drawImage(this.image, this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
        playgroundContext.restore();
    }
}